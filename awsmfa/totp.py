from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.twofactor.totp import TOTP
from cryptography.hazmat.primitives.hashes import SHA1
import time
import base64
import binascii


def generate_token(key):
    try:
        decoded = base64.b32decode(key)
    except binascii.Error:
        # The decryption key is wrong. Return a bogus token and let aws handle the error
        decoded =  base64.b32decode(''.join(['A' for i in range(64)]))

    token = TOTP(decoded, 6, SHA1(), 30,
                 default_backend()).generate(time.time())
    return token.decode('utf8')

